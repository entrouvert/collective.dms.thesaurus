from five import grok
from zope.interface import Interface
from plone.indexer import indexer


class IDmsKeywordIndexer(Interface):
    """Dexterity behavior interface for enabling the dynamic SearchableText
       indexer on Document objects."""


@indexer(IDmsKeywordIndexer)
def dmskeyword_searchable_text(obj):
    return obj.get_words_for_indexation()

grok.global_adapter(dmskeyword_searchable_text, name='SearchableText')
