#import datetime
from Acquisition import aq_parent
from zope.interface import implements

from zope import schema

from plone.dexterity.schema import DexteritySchemaPolicy
from plone.dexterity.content import Item

from plone.app.textfield import RichText

from plone.supermodel import model

from . import _
from .keywordsfield import ThesaurusKeywords
from .equivalencesfield import ThesaurusKeywordEquivalences
from .vocabulary import NoThesaurusFound

class IDmsKeyword(model.Schema):
    """ """

    title = schema.TextLine(
        title=_(u"Title")
        )

    # EQ: equivalences
    equivs = ThesaurusKeywordEquivalences(
        title=_(u'EQ (Equivalences)'),
        required=False,
        )

    # BT: broader term
    broader = ThesaurusKeywords(
        title=_(u"BT (Broader Terms)"),
        required=False,
        vocabulary=u'dms.thesaurus.internalrefs'
        )

    # RT: related term
    related = ThesaurusKeywords(
        title=_(u"RT (Related Terms)"),
        required=False,
        display_backrefs=True,
        vocabulary=u'dms.thesaurus.internalrefs'
        )

    # HN: historical note
    historical_note = RichText(
        title=_(u"HN (Historical Note)"),
        required=False,
        )

    # SN: scope note
    scope_note = RichText(
        title=_(u"SN (Scope Note)"),
        required=False,
        )


class DmsKeyword(Item):
    """ """
    implements(IDmsKeyword)

    def get_words_for_indexation(self, limit=5):
        thesaurus = aq_parent(self)
        words = []
        words.append(self.title)
        for word in self.equivs or []:
            words.append(word)
        if limit > 0:
            for broader_keyword in self.broader or []:
                try:
                    keyword = thesaurus[broader_keyword]
                except KeyError:
                    continue
                words.append(keyword.get_words_for_indexation(limit=limit-1))
        return ' '.join([x for x in words if x])

    def get_keyword_tree(self, limit=5):
        thesaurus = aq_parent(self)
        l = [self.id]
        for broader_keyword in self.broader or []:
            try:
                keyword = thesaurus[broader_keyword]
            except KeyError:
                continue
            l.extend(keyword.get_keyword_tree(limit=limit-1))
        return l


class DmsKeywordSchemaPolicy(DexteritySchemaPolicy):
    """ """

    def bases(self, schemaName, tree):
        return (IDmsKeyword, )

